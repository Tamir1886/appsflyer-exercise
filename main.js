/**
 *  converts a JavaScript object or value to a JSON string
 * @param obj 
 * @returns string 
 */

 function myStringify(obj) {

    let results="{"

    for (i = 0; i < Object.keys(obj).length; i++) {
        const [temp_key, temp_vaule] = [Object.keys(obj)[i], Object.values(obj)[i]]

        if (typeof (temp_vaule) == 'string') 
            results = results.concat(`"${temp_key}":"${temp_vaule}",`)
        
        else if (typeof (temp_vaule) == 'boolean' || typeof(temp_vaule) == 'number') 
            results = results.concat(`"${temp_key}":${temp_vaule},`)
        
        
        else  console.log('value not valid!')
    }
    //Add curly braces to the end of the results
    let jsonResult = results.substring(0, results.length - 1) + "}";
    return (jsonResult)
}

const dummy = { str: "fff", num: 5, bool: true };

console.log(JSON.stringify(dummy));
console.log(myStringify(dummy))
